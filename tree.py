# Түйіннің шешім ағашында болу болмауын анықтау.
class TreeNode:  # шешім ағашы класы
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None

root = TreeNode(10)  # түйін жасаймыз

root.left = TreeNode(5)  # тамыр сол жағы
root.right = TreeNode(15)  # тамыр оң жағы

root.left.left = TreeNode(3)  # Сол жақтың сол бөлігі
root.left.right = TreeNode(7)  # Оң жақтың сол бөлігі

root.right.left = TreeNode(12)  # Сол жақтың оң бөлігі
root.right.right = TreeNode(18)  # Оң жақтың оң бөлігі

def search(node, target): # Іздеу функциясы
    if not node:
        return None
    if node.value == target:
        return node
    elif node.value > target:
        return search(node.left, target)
    else:
        return search(node.right, target)

# Қолданылуы
result = search(root, 7)
if result:
    print(f"Түйін табылды мәні: {result.value}")
else:
    print("Түйін табылмады")