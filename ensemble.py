from sklearn.ensemble import GradientBoostingClassifier

# Градиенттік бустинг моделін құру
gbr = GradientBoostingClassifier(n_estimators=100, learning_rate=0.1, random_state=0)

# Деректер моделін оқыту
gbr.fit(X_train, y_train)

# Жаңа деректер үшін сыныптарды болжау
y_pred = gbr.predict(X_test)
